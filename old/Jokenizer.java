package jokenizer;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Jokenizer {
  public static void main(String[] args) throws IOException {
    StringReader sr = new StringReader("this is    an \t  example 123 string");
    Tokenizer t = new Tokenizer();
    final int WHITESPACE = t.add(RegexRule.whitespace());
    final int WORD = t.add(RegexRule.word());
    TokenizerRun tr = t.run(sr);
    int tok;
    while ((tok = tr.nextToken()) != 0) {
      if (tok == WHITESPACE) { System.err.println("WHITESPACE"); }
      if (tok == WORD) { System.err.println("WORD"); }
      System.err.println(">>" + tr.tokenText() + "<<");
    }
  }

  private static void test2() throws IOException {
    StringReader sr = new StringReader("hello world");
    ExplicitBufferedReader ebr = new ExplicitBufferedReader(sr);
    
    Pattern pat = Pattern.compile("\\w+");
    Matcher m = ebr.matcher(pat);
    System.out.println(m.lookingAt());
    System.out.println(m.end() - m.start());
  }
  
  public static void test() throws IOException {
    StringReader sr = new StringReader("hello world, this is an example string that has to be at least 50 characters long: I hope this is enough!");
    
    ExplicitBufferedReader ebr = new ExplicitBufferedReader(sr);
    
    while (ebr.getSize() >= 5) {
      System.out.println("buf:  " + new String(ebr.getArray()));
      System.out.println("start:" + ebr.getStart());
      System.out.println("end:  " + ebr.getFinish());
      System.out.println("size: " + ebr.getSize());
      System.out.println("str:  " + ebr.getString(5));
      System.out.println();
      ebr.consume(5);
    }
  }
}
