package jokenizer;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ExplicitBufferedReader {
  private final Reader reader;

  private final int MIN_SIZE = 10240;
  private final int MAX_SIZE = 40960;

  private final CharBuffer buf;
  private final char[] arr;
  public char[] getArray() { return arr; }
  public CharBuffer getBuffer() { return buf; }
  
  private int start = 0;
  private int finish = 0;
  public int getFinish() { return finish; }
  public int getStart() { return start; }
  public int getSize() {
    assert finish >= start;
    
    return finish - start;
  }

  private boolean eof = false;
  private void reloadBuffer() throws IOException {
    if (eof) return;
    
    final int size = getSize();
    
    if (size < MIN_SIZE) {
      System.arraycopy(arr, start, arr, 0, size);
      start = 0;
      finish = size;
      
      int len = reader.read(arr, finish, MAX_SIZE - finish);
      if (len == -1) {
        eof = true;
      } else {
        finish += len;
      }
    }
  }

  /** Attempt to match the regex to the beginning of the buffer. Return the
   *  matcher object. */
  public Matcher matcher(Pattern p) {
    Matcher m = p.matcher(buf);
    m.region(start, finish);
    return m;
  }
  
  public void consume(int len) throws IOException {
    if (len <= 0) throw new IllegalArgumentException("len must > 0");
    if (len > getSize()) throw new IllegalArgumentException("len must <= size");
    
    start += len;
    reloadBuffer();
  }
  
  public String getString(int len) {
    if (len < 0) throw new IllegalArgumentException("len must > 0");
    if (len > getSize()) throw new IllegalArgumentException("len must <= size");

    StringBuilder sb = new StringBuilder(len);
    sb.append(arr, start, len);
    return sb.toString();
  }
  
  ExplicitBufferedReader(Reader r) throws IOException {
    this.reader = r;
    arr = new char[MAX_SIZE];
    buf = CharBuffer.wrap(arr);
    reloadBuffer();
  }
}
