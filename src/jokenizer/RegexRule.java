package jokenizer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexRule extends Rule {
  final Pattern pattern;
  
  public RegexRule(String p) {
    this.pattern = Pattern.compile(p);
  }
  
   int match(ExplicitBufferedReader ebr) {
     Matcher m = ebr.matcher(pattern);
     if (m.lookingAt()) return m.end() - m.start();
     return 0;
   }
   
   public static RegexRule whitespace() {
     return new RegexRule("\\s+");
   }
   
   public static RegexRule word() {
     return new RegexRule("\\w+");
   }
}
