package jokenizer;

public abstract class Rule {
  /** If this is set, then tokens that much this rule will be ignored. */
  private boolean ignored;
  public boolean isIgnored() { return ignored; }
  public void setIgnored(boolean ignore) { this.ignored = ignore; }
  
  /** This is return by TokenizerRun.nextToken(). */
  int tokenInteger;
  public void setTokenInteger(int tokenInteger) { this.tokenInteger = tokenInteger; }
  
  /** Try to match a rule to the ExplicitBufferedReader and return number of characters consumed. */
  abstract int match(ExplicitBufferedReader ebr);
}
