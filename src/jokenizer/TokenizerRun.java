package jokenizer;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenizerRun {
  private final Tokenizer tokenizer;
  private final ArrayList<Rule> rules;
  private final ExplicitBufferedReader ebr;
  
  private int lastMatch;
  
  TokenizerRun(Tokenizer t, Reader r) {
    tokenizer = t;
    rules = tokenizer.getRules();
    try {
      ebr = new ExplicitBufferedReader(r);
    } catch (IOException ex) {
      throw new TokenizerError("error creating buffered reader");
    }
    lastMatch = 0;
  }

  /** Peek at next token. nextToken then rollBack. */
  public int peekToken() {
    int x = nextToken();
    rollBack();
    return x;
  }
  
  /** Roll back, so that nextToken will return the same token as last time.
   ** Roll backs are not infinite.  You can only go back once! */
  public void rollBack() {
    lastMatch = 0;
  }

  /** Consume until hitting this character */
  public void consumeUntil(String ch) {
    lastMatch = 0;
    
    Matcher m = ebr.matcher(Pattern.compile("[^" + ch + "]*"));
    if (m.lookingAt()) {
      try {
        ebr.consume(m.end() - m.start());
      } catch (IOException ex) {
        throw new TokenizerError("IOException while consumeUntil(" + ch + ")");
      }
    } else {
      throw new TokenizerError("consumeUntil(" + ch + ") failed");
    }
  }
  
  public int nextToken() {
    // Loop for tokens, this loop is needed because some tokens may be ignored.
    tokenloop: for(;;) {
      // If there was a match in the previous step, then consume it.
      if (lastMatch > 0) {
        try {
          ebr.consume(lastMatch);
          lastMatch = 0;
        } catch (IOException ex) {
          throw new TokenizerError("IOException while consuming buffered reader");
        }
      }
      
      // If nothing left, then EOF token (0).
      if (ebr.getSize() == 0) return 0;

      // Otherwise, iterate through the rules, and try to match the one that fits.
      for (Rule r : rules) {
        final int m = r.match(ebr);
        if (m > 0) {
          lastMatch = m;
          if (r.isIgnored()) continue tokenloop;
          else return r.tokenInteger;
        }
      }

      // If no rules match, then parse error!
      throw new TokenizerError("could not tokenize: '" + state() + "'");
    }
  }
  
  public String tokenText() {
    assert lastMatch >= 0;
    
    if (lastMatch == 0) {
      return "";
    }
    
    return ebr.getString(lastMatch);
  }
  
  public String state() {
    int sz = ebr.getSize();
    int n = Math.min(sz, 500);
    return ebr.getString(n);
  }
}
