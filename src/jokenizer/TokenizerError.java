package jokenizer;

public class TokenizerError extends RuntimeException {
  public TokenizerError(String message) {
    super(message);
  }
}
