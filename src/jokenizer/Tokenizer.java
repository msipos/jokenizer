package jokenizer;

import java.io.Reader;
import java.util.ArrayList;

public class Tokenizer {
  private ArrayList<Rule> rules;

  public ArrayList<Rule> getRules() { return rules; }
  
  private int counter = 0;

  public void add(Rule rule, int number) {
    rule.tokenInteger = number;
    rules.add(rule);
  }

  public int add(Rule rule) {
    counter++;
    add(rule, counter);
    return counter;
  }
  
  public void addIgnored(Rule rule) {
    rule.setIgnored(true);
    add(rule);
  }

  // Convenience methods for RegexRules that accept Strings.
  public void add(String rule, int number) {
    add(new RegexRule(rule), number);
  }
  
  public int add(String rule) {
    return add(new RegexRule(rule));
  }
  
  public void addIgnored(String rule) {
    addIgnored(new RegexRule(rule));
  }
  
  public Tokenizer() {
    rules = new ArrayList<Rule>();
  }
  
  public TokenizerRun run(Reader r) {
    return new TokenizerRun(this, r); 
  }
}
